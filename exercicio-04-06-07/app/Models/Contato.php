<?php

namespace Conam\Models;

use Illuminate\Database\Eloquent\Model;

class Contato extends Model
{
    protected $table="contatos";
    protected $primaryKey = 'id_contato';
    protected $guarded = ['id_contato'];
    protected $dates = [
        'created_at',
        'updated_at',
    ];
    /**
     * Obtém os telefones relacionados ao Contato
     */
    public function telefones()
    {
        return $this->hasMany(Telefone::class, 'id_contato', 'id_contato');
    }
}
