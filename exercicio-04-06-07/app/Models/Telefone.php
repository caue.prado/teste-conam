<?php

namespace Conam\Models;

use Illuminate\Database\Eloquent\Model;

class Telefone extends Model
{
    protected $table="telefones";
    protected $primaryKey = 'id_telefones';
    protected $guarded = ['id_telefones'];
    protected $dates = [
        'created_at',
        'updated_at',
    ];
    /**
     * Obtém os telefones relacionados ao Contato
     */
    public function contatos()
    {
        return $this->belongsTo(Telefone::class, 'id_contato', 'id_contato');
    }
}
