<?php

namespace Conam\Http\Controllers\Api;

use Illuminate\Http\Request;
use Conam\Http\Controllers\Controller;
use Illuminate\Http\Response;

class AgendaControllerApi extends Controller
{
    public function listarTelefones(Request $request)
    {
        try {
            $nome = $request->query('nome');
            $tipo = $request->query('tipo');

            $service = new \Conam\Services\AgendaService();
            $telefones = $service->listarTelefones($nome, $tipo);
            return response()->json([
                'ok' => true,
                'telefones' => $telefones
            ], Response::HTTP_OK);
        } catch (Exception $exception) {
            return response()->json([
                'ok' => false,
                'message' => $exception->getMessage(),
            ], \Illuminate\Http\Response::HTTP_BAD_REQUEST);
        }
    }
}
