<?php

namespace Conam\Services;

use Conam\Models\Telefone;

class AgendaService
{
    protected $telefoneModel;

    public function __construct()
    {
        $this->telefoneModel = new Telefone();
    }

    public function listarTelefones($nome = null, $tipo = null)
    {
        return $this->telefoneModel
            ->join('contatos', 'telefones.id_contato', '=', 'contatos.id_contato')
            ->where("telefones.tipo", $tipo)
            ->where("contatos.nome", $nome)
            ->get()
            ->toArray();
    }
}
