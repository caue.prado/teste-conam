<?php

use Illuminate\Database\Capsule\Manager;

$mng = new Manager();
$mng->addConnection([
    'driver' => 'mysql',
    'host' => 'mysql',
    'port' => 3306,
    'database' => 'conam',
    'username' => 'conam',
    'password' => 'conam@mysql',
    'charset' => 'utf8',
    'collation' => 'utf8_unicode_ci',
], 'default');

$mng->bootEloquent();
$mng->setAsGlobal();