<?php
use App\Database\Migrations\Migration;

class ContatoMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schema->create('contato', function (Illuminate\Database\Schema\Blueprint $table) {
            // Auto-increment id
            $table->increments('id_contato');
            $table->string('nome', 250);
            $table->date('data_nascimento');

            // Required for Eloquent's created_at and updated_at columns
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->schema->drop('contato');
    }
}