<?php

namespace App\Database\Migrations;

use Illuminate\Database\Capsule\Manager as Capsule;
use Phinx\Migration\AbstractMigration;

class Migration extends AbstractMigration {

    public $capsule;
    public $schema;

    public function init() {
        $this->capsule = new Capsule();
        $this->capsule->addConnection([
          'driver' => 'mysql',
            'host' => 'mysql',
            'port' => 3306,
            'database' => 'conam',
            'username' => 'conam',
            'password' => 'conam@mysql',
            'charset' => 'utf8',
            'collation' => 'utf8_unicode_ci',
        ], 'default');

        $this->capsule->bootEloquent();
        $this->capsule->setAsGlobal();
        $this->schema = $this->capsule->schema();
    }

}