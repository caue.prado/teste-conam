<?php
use App\Database\Migrations\Migration;

class TelefoneMigration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $this->schema->create('telefones', function (Illuminate\Database\Schema\Blueprint $table) {
            // Auto-increment id
            $table->increments('id_telefones');
            $table->integer('id_contato');
            $table->string('ddd', 3);
            $table->string('telefone', 8);
            $table->string('tipo', 50);
            // Required for Eloquent's created_at and updated_at columns
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $this->schema->drop('telefones');
    }
}