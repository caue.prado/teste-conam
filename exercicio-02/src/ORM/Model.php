<?php

namespace CPrado\ORM;

use CPrado\Drivers\DatabaseInterface;

abstract class Model
{
    protected $driver;

    public function setDriver(DatabaseInterface $driver)
    {
        $this->driver = $driver;
        $this->driver->setTable($this->table);
        return $this;
    }

    protected function getDriver()
    {
        return $this->driver;
    }

    public function save()
    {
        $this->getDriver()
            ->save($this)
            ->exec();
    }

    public function findAll(array $conditions = [])
    {
        $result = $this->getDriver()
            ->select($conditions)
            ->exec()
            ->all();

        return is_null($result)? []:$result;
    }

    public function findFirst($id)
    {
        $result = $this->getDriver()
            ->select(['id' => $id])
            ->exec()
            ->first();

        return is_null($result)? []:$result;
    }

    public function delete()
    {
        $this->getDriver()
            ->delete(['id' => $this->id])
            ->exec();
    }

    public function __get($variable)
    {
        if ($variable === 'table') {
            $table = get_class($this);
            $table = explode('\\', $table);
            return strtolower(array_pop($table));
        }
        return null;
    }
}