<?php

namespace App\Controllers;

use App\Model\Telefones;

class TelefoneController
{
    protected $telefoneModel;

    public function __construct()
    {
        $telefoneModel = new Telefones();
        $this->telefoneModel = $telefoneModel;
    }

    public function getTelefones($tipo = null)
    {
        $result = $this
            ->telefoneModel::where('tipo', $tipo)->get()->toArray();

        return $result;
    }
}