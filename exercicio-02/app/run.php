<?php

// configuração inicial
require __DIR__ . '/../vendor/autoload.php';
include __DIR__ . '/../bootstrap.php';

use App\Controllers\TelefoneController;
use App\Model\Telefones;
use App\Model\Contatos;

$contatos = new Contatos();
$contatos->nome = 'Caue';
$contatos->data_nascimento = '1994-02-23';
$contatos->save();
print_r('salvando primeiro contato' . PHP_EOL);

$model = new Telefones();
$model->id_contato = 1;
$model->ddd =  11;
$model->telefone = '98128-7535';
$model->tipo = 'celular';
$model->save();
print_r('salvando telefone do contato' . PHP_EOL);


$instance = new TelefoneController();
print_r($instance->getTelefones('celular'));