Conam - Exercises
===================

O projeto Conam � um software aberto, desenvolvido pelo Cau� Prado, com intuito de resolver os exerc�cios proposto pela empresa Conam.

Social Media:
```
Github: caueprado0
Instagram: @sigaocaue
```
____

## Criar migrations
Para criar uma migration basta:
```
 php vendor/bin/phinx create --template database/migrations/stubs/create.stub <nome-migration>
```
Para rodar uma migration basta:
```
 php vendor/bin/phinx migrate -e dev
```