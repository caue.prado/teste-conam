<?php

$original = array ("A1", "A2", "A3", "B1", "B2", "B3", "C1", "C2", "C3");
$result = array();

for ($i = 0; $i < count($original); $i++) {
    $numberFromString = null;
    preg_match("/([0-9]+)/", $original[$i], $numberFromString);
    $numberFromString = is_array($numberFromString)?intval($numberFromString[0]):null;

    if (is_int($numberFromString)) {
        $result[$numberFromString] = is_array($result[$numberFromString])?$result[$numberFromString]:array();
        array_push($result[$numberFromString], $original[$i]);
    }
}

print_r($result);
