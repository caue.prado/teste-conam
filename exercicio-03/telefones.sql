create table telefones
(
    id_telefones int unsigned auto_increment
        primary key,
    id_contato   int         not null,
    ddd          varchar(3)  not null,
    telefone     varchar(8)  not null,
    tipo         varchar(50) not null,
    created_at   timestamp   null,
    updated_at   timestamp   null
)
    collate = utf8_unicode_ci;


