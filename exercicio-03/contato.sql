create table contato
(
    id_contato      int unsigned auto_increment
        primary key,
    nome            varchar(250) not null,
    data_nascimento date         not null,
    created_at      timestamp    null,
    updated_at      timestamp    null
)
    collate = utf8_unicode_ci;


